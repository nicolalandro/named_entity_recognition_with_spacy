import pickle
import pandas as pd
import re

from tqdm import tqdm

csv_pah = '../datasets/gaerne_tp.csv'
labels_set = ['product']

if __name__ == '__main__':
    x = 'ysada'
    print(f"\033[92m {x}\033[00m")
    exit()

    df = pd.read_csv(csv_pah)
    docs = []
    for i, row in df.iterrows():
        docs.append((row['title'] + ' ' + row['description']).lower())
    data = []
    occurrencies = 0
    for text in docs:
        entities = []
        word = 'stivali'
        for index in re.finditer(word, text.lower()):
            entities.append([index.start(), index.end(), labels_set[0]])
            occurrencies += 1
        data.append((text, {'entities': entities}))
    print(word, ':', occurrencies)

    nlp = pickle.load('../models/ner.save')

    for elem in data:
        text = elem[0]
        doc = nlp(text)
        old_index = 0
        colored_string = ''
        for x in doc.ents:
            # print('\t', x.start, x.end, x.label_)
            colored_string += text[old_index:x.start - 1]
            colored_string += f"\033[92m {text[x.start:x.end]}\033[00m"
            old_index = x.end + 1
        print(colored_string)
