import re
import pandas as pd
import spacy
from spacy.util import compounding, minibatch
from tqdm import tqdm
import random
import pickle

csv_pah = '../datasets/gaerne_tp.csv'
labels_set = ['product']

# paramiters
epochs = 30

if __name__ == '__main__':
    df = pd.read_csv(csv_pah)
    docs = []
    for i, row in df.iterrows():
        docs.append((row['title'] + ' ' + row['description']).lower())
    data = []
    occurrencies = 0
    for text in docs:
        entities = []
        word = 'stivali'
        for index in re.finditer(word, text.lower()):
            entities.append([index.start(), index.end(), labels_set[0]])
            occurrencies += 1
        data.append((text, {'entities': entities}))
    print(word, ':', occurrencies)

    # create the model
    nlp = spacy.blank("it")
    ner = nlp.create_pipe("ner")
    nlp.add_pipe(ner)
    for label in labels_set:
        ner.add_label(str(label))
    optimizer = nlp.begin_training()

    # train
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "ner"]
    with nlp.disable_pipes(*other_pipes):  # only train NER
        sizes = compounding(1.0, 4.0, 1.001)
        # batch up the examples using spaCy's minibatch
        for itn in tqdm(range(epochs), desc='Train'):
            random.shuffle(data)
            batches = minibatch(data, size=sizes)
            losses = {}
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer,
                           drop=0.35, losses=losses)
    # pickle.dump('../models/ner.save', nlp)
    # pickle.dumps(nlp, 1)
    # pickle.loads(data)
    for elem in data:
        text = elem[0]
        doc = nlp(text)
        old_index = 0
        colored_string = ''
        for x in doc.ents:
            print(x.start, x.end)
            # print('\t', x.start, x.end, x.label_)
            colored_string += text[old_index:x.start]
            colored_string += f"\033[92m{text[x.start:x.end]}\033[00m"
            old_index = x.end
        if old_index < len(text) - 1:
            colored_string += text[old_index:len(text)]
            print(colored_string)
